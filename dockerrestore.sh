#!/bin/bash
clear
tput bold; cat header; tput sgr0
echo ""
echo "______________________________________________________________________________"
echo ""
tput bold; echo "This is a simple script that back up docker containers to images "
echo "Usage: dockerbackup.sh <container-id>"; tput sgr0
echo "______________________________________________________________________________"
echo ""
echo "Author: Ihsan Izwer                                              Version: 1.0"
echo "______________________________________________________________________________"
echo ""


docker load -i ~/${1}